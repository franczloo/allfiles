import sys
import os
import ujson
import json
import collections
import bz2
import math
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon
import numpy as np
import glob


'''Here this script gets a bz2 file from the cloud, processes it
saves it into a json, and deletes the bz2 file once it's done.
'''

cities = (
("New York City", 40.730610, -73.935242),
("Los Angeles", 34.052235, -118.243683),
("Houston", 29.766083, -95.358810),
("Philadelphia", 39.952583, -75.165222),
("Phoenix", 33.448376, -112.074036),
("Denver", 39.742043, -104.991531),
("Chicago", 41.881832, -87.623177),
("Seattle", 47.608013, -122.335167),
("Cleveland", 41.505493, -81.681290),
("Charlotte", 35.227085, -80.843124),
("Jacksonville", 30.332184, -81.655647),
("Washington DC", 38.889931, -77.009003),
("Memphis", 35.002865, -89.997658),
("Nashville", 36.174465, -86.767960),
("Baltimore", 39.299236, -76.609383),
("Las Vegas", 36.114647, -115.172813),
("Tuscon", 32.253460, -110.911789),
("Atlanta", 33.753746, -84.386330),
("Miami", 25.761681, -80.191788),
("New Orleans", 29.951065, -90.071533),
("Tulsa", 36.153980, -95.992775),
("Milwaukee", 43.038902, -87.906471),
("Indianapolis", 39.771854, -86.157013),
("Wichita", 37.697948, -97.314835),
("Boston", 42.3601, -71.0589)
)

keyWords = [['bernie','sanders'],
            ['hillary','clinton'],
            ['barack','obama'],
            ['democrat'],
            ['republican'],
            ['caucus'],
            ['election'],
            ['primary'],
            ['general'],
            ['donald','trump']]

bigHash = {}


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1 = math.radians(lon1)
    lat1 = math.radians(lat1)
    lon2 = math.radians(lon2)
    lat2 = math.radians(lat2)

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = (math.sin(dlat/2)**2) + (math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2)
    c = 2 * math.asin(math.sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r


def getSet(tweet):
    myset = set()
    exclude = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    no_punct = ""
    for char in tweet:
        if char not in exclude:
            no_punct = no_punct + char
    no_punct = no_punct.lower()
    parse = no_punct.split()
    for word in parse:
        myset.add(word)
    return myset

def setList(tweet):
    myList = []
    for word in tweet:
        if word in keyWords:
            myList.append(word)
    return myList

def getDate(tweet):
    dates = ''
    dates += str(tweet['created_at'][4:10])
    dates += ' '
    dates += str(tweet['created_at'][26:30])
    return dates

fileJ = sys.argv[1]
count = int(fileJ)

try:
    num = format(count, '009d')
    strStart = "wget https://s3.amazonaws.com/twitter-all-archive/data-"
    strEnd = ".json.bz2"
    arg = strStart + num + strEnd
    fileName = arg[-23:]
    os.system(arg)
    print(fileName)

    if fileName.endswith('bz2'):
        fileNew = bz2.BZ2File(fileName).read()
        decompressed = json.loads(fileNew)
        for c in decompressed:
            coord = c["coordinates"]
            #print(coord)
            lat = None
            long = None
            if coord is not None:
                loc = coord['coordinates']
                long = float(loc[0])
                lat = float(loc[1])
            if c["place"] is not None and c["place"]["bounding_box"] is not None and c["place"]["bounding_box"]["coordinates"] is not None:
                lat = 0.0
                long = 0.0
                count = 0
                for pair in c["place"]["bounding_box"]["coordinates"][0]:
                    long += pair[0]
                    lat += pair[1]
                    count += 1
                lat /= count
                long /= count
            if lat != None and long != None:
                string = ""
                latMath = round(lat,1)
                if int(10*latMath) % 2 != 0:
                    longMath = round(long,1)
                else:
                    longMath = math.floor(10*long)/10 + .05
                string += str(longMath)
                string += " "
                string += str(latMath)
            #print(string)
            #print(long,lat)
            myTweet = c['text']
            date = getDate(c)
            returnedSet = getSet(myTweet)
            for (city, latitude, longitude) in cities:
                dist = haversine(long,lat,longitude,latitude)
                #print(dist, long,lat,longitude, latitude)
                if dist <= 300:
                    #print(returnedSet)

                    if city not in bigHash:
                        bigHash[city] = {}
                    for k in keyWords:
                        anyTrue = False
                        for w in k:
                            if w in returnedSet:
                                anyTrue = True
                                break

                        if anyTrue:
                            key = k[0]
                            if key not in bigHash[city]:
                                bigHash[city][key] = {}
                            if date in bigHash[city][key]:
                                bigHash[city][key][date] += 1
                            else:
                                bigHash[city][key][date] = 1
                        if "total" not in bigHash[city]:
                            bigHash[city]["total"] = {}
                        if date in bigHash[city]["total"]:
                            bigHash[city]["total"][date] += 1
                        else:
                            bigHash[city]["total"][date] = 1
    os.remove(fileName)
    with open(fileName + 'cities.json','w') as outfile:
        print("finished",outfile)
        ujson.dump(bigHash, outfile)

except:
    print("Your file number is not found in this folder, max files reached")
    raise



