import sys
import os
import ujson
import json
import collections
import bz2
import math
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon
import numpy as np
import glob

count = 0
for file in os.listdir('.'):
    if file.endswith(".csv .png"):
        count += 1 
        print(count, file)
        os.remove(file)
