#!usr/bin/env Rscript
#install.packages("ggplot2")
library(ggplot2)
file.names <- list.files(path = ".",pattern = ".csv",full.names=TRUE)
count <- 0
for (file in file.names){
    count <- count + 1
    dat <- read.csv(file)
    print(dat)
    p <- ggplot(dat, aes(Date,percent, group=tag,color=tag)) + theme(text=element_text(size=5)) + geom_point() + geom_line()
    p <- p + theme(axis.text.x = element_text(angle=90,hjust=1))
    name <- paste(file,".png")
    ggsave(name,p)
    
}
print(count)
