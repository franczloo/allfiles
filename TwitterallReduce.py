import sys
import string
import os
import ujson
import json
import collections
import bz2
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon
import numpy as np
import glob
import pandas as pd


bigHash = {}
count = 0
print ("Grabbing data from files")
for filename in glob.glob("data-*.bz2cities.json"):
    count += 1
    print (count, filename)
    with open(filename) as json_data:
        lhash = ujson.loads(json_data.read())
        for city in lhash:
            if city not in bigHash:
                bigHash[city] = {}
            for k in lhash[city]:
                if k not in bigHash[city]:
                    bigHash[city][k] = {}
                for date in lhash[city][k]:
                    if date not in bigHash[city][k]:
                        bigHash[city][k][date] = lhash[city][k][date]
                    else:
                        bigHash[city][k][date] += lhash[city][k][date]
       
#print (bigHash)

#getting the percents to dates and ordering them to be plotted
import datetime
from datetime import date

def monthToNum(shortMonth):
    return{
           'Jan':1,
           'Feb':2,
           'Mar':3,
           'Apr':4,
           'May':5,
           'Jun':6,
           'Jul':7,
           'Aug':8,
           'Sep':9,
           'Oct':10,
           'Nov':11,
           'Dec':12
           }[shortMonth]

for city in bigHash:
    bigFrame = pd.DataFrame()
    for val in bigHash[city]:
        percent_hash = {}
        if 'total' in bigHash[city]:
            tot = bigHash[city]['total']
        if val != 'total':
            for word in bigHash[city][val]:
                month = monthToNum(word[:3])
                date = str(month) + word[3:]
                month = date[:2]
                if date[2] == '0':
                    day = date[3:5]
                else: 
                    day = date[2:5]
                year = date[5:]
                myDate = datetime.date(int(year),int(month),int(day))

                percent = (bigHash[city][val][word] * 1.0)/ float(tot[word])
                percent_hash[myDate] = percent
            sort_hash = collections.OrderedDict(sorted(percent_hash.items()))
            x = pd.Series(list(sort_hash.keys()))
            xrange = list(range(0,len(x)))
            for i in range(0, len(x)):
                x[i] = str(x[i].strftime('%m/%d/%Y'))
            y = pd.Series(list(sort_hash.values()))
     
            #print("Values: ", val, "x ticks: ",x,' Y: ',y,' x ', xrange)
            df = pd.DataFrame({'x':xrange,'y':y,'Ticks':x})
            dfNew = pd.DataFrame({'Date':x,'tag':val,'percent':y})
            bigFrame = bigFrame.append(dfNew)
        name = city + '.csv'
        print(bigFrame)
        if not len(bigFrame) == 0:
            bigFrame.to_csv(name) 
            
